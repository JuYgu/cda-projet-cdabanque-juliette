package fr.afpa.control;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.afpa.bean.Utilisateur;
import fr.afpa.bean.Compte;
import fr.afpa.bean.Operation;
import fr.afpa.bean.Client;
import fr.afpa.model.GAgence;
import fr.afpa.model.GClient;
import fr.afpa.model.GCompte;
import fr.afpa.model.GOperation;
import fr.afpa.model.GUtilisateur;
import fr.afpa.model.Print;
import fr.afpa.view.Display;


public interface Control  {

	public static GUtilisateur gestionUtilisateur = new GUtilisateur(); 
	public static GAgence gestionAgence = new GAgence(); 
	public static GClient gestionClient = new GClient(); 
	public static GCompte gestionCompte = new GCompte();
	public static GOperation gestionOperation = new GOperation(); 
	
	
	
	/**
	 * V�rifie que l'utilisateur �xiste bien
	 * @param codeUtilisateur
	 * @return
	 */
	public static String [] auth(String codeUtilisateur) {
		String [] retour = new String [2];
		if (analyseLogin(codeUtilisateur)) {
			Utilisateur user = gestionUtilisateur.isExist(codeUtilisateur);
			retour[0] = user.getId_utilisateur();
			retour[1] = user.getId_typeUtilisateur()+"";
			
		}
		return retour;
	}
	

	
	/**
	 * Analyse la syntaxe du login tap� par le client
	 * @param login
	 * @return boolean
	 */
	public static boolean analyseLogin(String login) {
		System.out.println("login");
		String [] pattern = new String [3];
		pattern[0] = "[1-9][0-9]{9}"; 
		pattern[1] = "CO[0-9]{4}"; 
		pattern[2] = "ADM[0-9]{2}";
		
		for (int i = 0; i< pattern.length; i++) {
			Pattern p = Pattern.compile(pattern[i]);
			Matcher m = p.matcher(login);
			if (m.matches()) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * V�rie que le noClient donn� par l'utilisateur existe bien
	 * @param noClient
	 * @return
	 */
	public static Client checkNoClient(String noClient) {
		Client newClient = null;
		if(analyseNoClient(noClient)) {
			newClient = gestionClient.getClientById(noClient);
		}
		
		return newClient;
	}
	
	/**
	 * Annalyse la syntaxe donn�e par l'user
	 * @param noClient
	 * @return
	 */
	public static boolean analyseNoClient(String noClient) {
		Pattern p = Pattern.compile("[A-Z]{2}[0-9]{6}");
		Matcher m = p.matcher(noClient);
		if (m.matches()) {
			return true;
		}
	
	return false;
	}
	
	/**
	 * Permet d'analyser si la syntaxt est respect�e
	 * @param noCompte
	 * @return
	 */
	public static boolean analyseNoCompte(String noCompte) {

		Pattern p = Pattern.compile("[0-9]{10}");
		Matcher m = p.matcher(noCompte);
		if (m.matches()) {
			return true;
		}
	
	return false;
}

	
	
	/**
	 * Permer de cr�er une agence
	 * @param paramAgence
	 * @return
	 */
	public static boolean createAgence(String paramAgence) {
		return gestionAgence.createAgence(paramAgence);
	}
	
	/**
	 * Permet de cr�er un client
	 * @param idAgence
	 * @param infoClient
	 * @return
	 */
	public static String createClient(int idAgence, String infoClient) {
		// Cr�ation du compte utiliateur et r�up de l'id utilisateur cr�e
		String idUser = gestionUtilisateur.createUtilisateur(3);
		
		//Cr�ation du client
		return  gestionClient.createClient(infoClient, idAgence, idUser)+";"+idUser;
		
	}
	
	/**
	 * Permet la cr�ation d'un compte
	 * @param typeCompte
	 * @param id_noClient
	 * @param decouvert
	 * @return
	 */
	public static boolean createCompte(int typeCompte, String id_noClient, boolean decouvert) {
		return gestionCompte.createCompte(typeCompte, id_noClient, decouvert);
	}
	
	/**
	 * Permet d'alimenter un compte
	 * @param listCompte
	 * @param choixCompteCrediteur
	 * @param montant
	 * @return
	 */
	public static boolean alimenterCompte(List<Compte>  listCompte, int choixCompteCrediteur, float montant) {
		GOperation gestionOperation = new GOperation(); 
		Operation opeCred = gestionOperation.creerOperation(montant, listCompte.get(choixCompteCrediteur-1), false);
		return gestionCompte.appro(listCompte.get(choixCompteCrediteur-1), opeCred);
	}
	
	
	/**
	 * Permet de faire un retrait
	 * @param listCompte
	 * @param choixCompteDebiteur
	 * @param montant
	 * @return
	 */
	public static boolean retrait(List<Compte> listCompte, int choixCompteDebiteur, float montant) {
		
			Operation opeDeb = gestionOperation.creerOperation(montant, listCompte.get(choixCompteDebiteur-1), true);
			return gestionCompte.retrait(listCompte.get(choixCompteDebiteur-1), opeDeb);
			
	}
	
	/**
	 * Permet de d�sactiver un compte
	 * @param noCompte
	 * @return
	 */
	public static boolean desactivation(String noCompte, int activ) {
		if(analyseNoCompte(noCompte)) {
			return gestionCompte.desactiver(noCompte,  activ);
		}
		return false;
	}
	
	/**
	 * Permet de modifier la domiciliation du client
	 * @param nomAgence
	 * @param noClient
	 * @return
	 */
	public static boolean changeAgence(String noClient, String nomAgence ) {
		if(analyseNoClient(noClient) && GClient.getClientById(noClient) != null) {
			int [] result = GAgence.findIdAgence(nomAgence);
			if(result[0] != -1) {
				return GClient.update(noClient, result[0]+"", 4);
			}
		}
		return false;
	}
	
	/**
	 * Permet de modifier le client
	 * @param noClient
	 * @param champsToUpdate
	 * @param choix
	 * @return
	 */
	public static boolean update(String noClient, String champsToUpdate,  int choix) {
		if(analyseNoClient(noClient) && GClient.getClientById(noClient) != null) {
			return GClient.update(noClient, champsToUpdate, choix);
		}
		return false;
	}

	/**
	 * Permets de v�rifier si le solde du compte permets de faire un virement
	 * @param montant
	 * @param compte
	 * @return
	 */
	public static boolean checkMontantCompte(float montant, Compte compte) {
		if (montant > compte.getSolde() && !compte.isAuth_decouvert()) {
			return false;
		}
		return true;
	}
	
	public static void print(Client client, List<Compte> listCompte) {

				Print.printPdf(client, listCompte);
			

			
		
	}
	
	/**
	 * Permet de v�rifier si une agence existe d�j�
	 * @param nomAgence
	 * @return
	 */
	public static int [] findAgence(String nomAgence) {
		return gestionAgence.findIdAgence(nomAgence);
	}
	
}
