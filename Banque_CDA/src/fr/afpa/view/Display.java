package fr.afpa.view;

import java.util.List;
import java.util.Scanner;

import fr.afpa.bean.Client;
import fr.afpa.bean.Compte;
import fr.afpa.bean.Operation;
import fr.afpa.control.Control;
import fr.afpa.model.GOperation;

public class Display implements Control {

	
	
	public Display() {
		dispatchRole(auth());
	}
	
	/**
	 * Permet l'authentification de l'utilisateur et charge son role
	 * @return
	 */
	public static String [] auth() {
		System.out.println("Bienvenue a la Banque CDA \n"
						 + "Merci de taper votre code Utilisateur ");
		Scanner in = new Scanner (System.in);
		String codeUtilisateur = in.next();
		
		String [] role = Control.auth(codeUtilisateur);
		
		if (role[0] == null) {
			System.out.println("Votre login n'est pas reconnu, merci de recommencer");
			auth();
		}
		
		return role;
		
	}
	
	
	/**
	 * R�cup�re le role de l'utilisateur endrant et lui dispatche ses menu
	 * @param role
	 */
	public static void dispatchRole(String [] role) {
		System.out.println(role[1]);
		if ("1".equals(role[1])) {
			displayAdmin();
		}else if("2".equals(role[1]) ) {
			displayConseiller();
		}else if ("3".equals(role[1])) {
			displayClient(role[0]);
		}	
	}
	
	
	/**
	 * Affiche le menu de l'admin et renvoie vers le control de ses choix
	 */
	public static void displayAdmin() {
		System.out.println("--------------------------------------------------------------\n"
				 + "1 - Consulter un profil\n"
				 + "2 - Consulter comptes\n"
				 + "3 - Consulter op�rations de comptes\n"
				 + "4 - Faire un virement\n"
				 + "5 - imprimer un relev� de compte\n"
				 + "6 - alimenter compte\n"
				 + "7 - faire un retrait \n"
				 + "8 - Creer un compte\n"
				 + "9 - Creer client\n"
				 + "10 - Changer domicilitation d'un client\n"
				 + "11 - Modifier les informations d'un client\n"
				 + "12 - Cr�er une agence\n"
				 + "13 - Activer/D�sactiver un compte\n"
				);
		
		Scanner in = new Scanner (System.in);
		int choix = in.nextInt();
		
		if(choix  < 8) {
			/*Demander code Client*/
			String noClient = askNoClient();
			Client client = gestionClient.getClientById(noClient);
			List <Compte> listCompte = gestionClient.getCompte(client);
			listCompte = gestionCompte.getOperation(listCompte);
			if(choix == 1) {
				displayClientProfil(client);
			} else if (choix == 2) {
				if (listCompte != null) {
					displayClientCompte(listCompte);
				}else {
					displayErrorEmptyCompte();
				}
			} else if (choix == 3) {
				// Consulter op�ration compte
				consultOperationCompte(listCompte);
			} else if (choix == 4) {
				// Faire virement
				createVirement(listCompte);	
			} else if (choix == 5) {

				print(client, listCompte);
				// Imprimer relev� de compte
			}else if (choix == 6) {
				// Alimenter Compte
				alimenterCompte(listCompte);
			} else if (choix == 7) {
				// Faire un retrait
				retrait(listCompte);
			}
			
		} else if (choix == 8) {
			// Cr�er Compte
			createCompte();
		} else if (choix == 9) {
			// Cr�er Client
			createClient();
		} else if (choix == 10) {
			// Changer domiciliation (agence)
			changeAgence();
		} else if (choix == 11) {
			// Modifier info Client
			changeInfoClient();
		} else if (choix == 12) {
			in.nextLine();
			System.out.println("Merci d'indiquer le nom et l'adresse de l'agence en s�parant les deux par un point vigule ");
			String paramAgence = in.nextLine();
			if (Control.createAgence(paramAgence)) {
				System.out.println("L'agence a bien �t� cr��e");
			}
		} else if (choix == 13) {
			// D�sactiver Compte
			String noCompte = askNoCompteToDesactivate();
			int activ = askActiveOrNot();
			if(Control.desactivation(noCompte, activ)) {
				System.out.println("Le compte a bien �t� d�sactiv�");
			}else {
				System.out.println("Le no de compte est erron�");
			}

		}
		
		displayAdmin();
	}
	
	/**
	 * Affiche le menu du conseiller et renvoie vers le control de ses choix
	 */
	public static void displayConseiller() {
		System.out.println("--------------------------------------------------------------\n"
				 + "1 - Consulter un profil\n"
				 + "2 - Consulter comptes\n"
				 + "3 - Consulter op�rations de comptes\n"
				 + "4 - Faire un virement\n"
				 + "5 - imprimer un relev� de compte\n"
				 + "6 - alimenter compte\n"
				 + "7 - faire un retrait \n"
				 + "8 - Creer un compte\n"
				 + "9 - Creer client\n"
				 + "10 - Changer domicilitation d'un client\n"
				 + "11 - Modifier les informations d'un client\n");
		
		Scanner in = new Scanner (System.in);
		int choix = in.nextInt();
		
		if(choix  < 8) {
			/*Demander code Client*/
			String noClient = askNoClient();
			Client client = gestionClient.getClientById(noClient);
			List <Compte> listCompte = gestionClient.getCompte(client);
			listCompte = gestionCompte.getOperation(listCompte);
			if(choix == 1) {
				displayClientProfil(client);
			} else if (choix == 2) {
				if (listCompte != null) {
					displayClientCompte(listCompte);
				}else {
					displayErrorEmptyCompte();
				}
			} else if (choix == 3) {
				// Consulter op�ration compte
				consultOperationCompte(listCompte);
			} else if (choix == 4) {
				// Faire virement
				createVirement(listCompte);	
			} else if (choix == 5) {
				// Imprimer relev� de compte
				print(client,listCompte);
			}else if (choix == 6) {
				alimenterCompte(listCompte);
				
			} else if (choix == 7) {
				// Faire un retrait
				retrait(listCompte);
			}	
			
		} else if (choix == 8) {
			createCompte();
		} else if (choix == 9) {
			createClient();
		} else if (choix == 10) {
			// Changer domiciliation (agence)
			changeAgence();
		} else if (choix == 11) {
			// Modifier info Client
			changeInfoClient();
		} 
		displayConseiller();
		
	}
	
	/**
	 * Affiche le menu du client et renvoie vers le control de ses choix
	 */
	public static void displayClient(String idUtilisateur) {
		System.out.println("--------------------------------------------------------------\n"
						 + "1 - Consulter profil\n"
						 + "2 - Consulter comptes\n"
						 + "3 - Consulter op�rations de comptes\n"
						 + "4 - Faire un virement\n"
						 + "5 - imprimer un relev� de compte\n"
						 + "6 - alimenter compte\n"
						 + "7 - faire un retrait \n");
		
		Client client = gestionClient.getClient(idUtilisateur);
		Scanner in = new Scanner (System.in);
		int choix = in.nextInt();
		List <Compte> listCompte = gestionClient.getCompte(client);
		listCompte = gestionCompte.getOperation(listCompte);
		if(choix == 1) {
			displayClientProfil(client);
		} else if (choix == 2) {
			if (listCompte != null) {
				displayClientCompte(listCompte);
			}else {
				displayErrorEmptyCompte();
			}
		} else if (choix == 3) {
			// Consulter op�ration compte
			consultOperationCompte(listCompte);
		} else if (choix == 4) {
			// Faire virement
			createVirement(listCompte);
			
		} else if (choix == 5) {
			// Imprimer relev� de compte
			print(client, listCompte);
		}else if (choix == 6) {
			// Alimenter Compte
			alimenterCompte(listCompte);
		} else if (choix == 7) {
			// Faire un retrait
			retrait(listCompte);

		} 
		
		displayClient(idUtilisateur);
	}
	
	
	
	
	/*--------------------------------------CREATION CLIENT / COMPTE----------------------------------------------*/
	/**
	 * Affichage permettant de cr�er un client
	 */
	public static void createClient() {
		String nomAgence = askAgence();
		int [] idAgence = Control.findAgence(nomAgence);
		if(idAgence[1] == 0) {
			System.out.println("L'agence n'existe pas, merci de recommencer");
			createClient();
		}
		String infoUser = askInfoUser();
		
		String infoClient = Control.createClient(idAgence[0], infoUser);
		
		System.out.println("Le client a bien �t� cr�� : Son num�ro client :"+infoClient.split(";")[0]
						 + "\nVoici son login pour pouvoir se connecter � son compte : "+infoClient.split(";")[1]);
	}
	
	public static void createCompte() {
		String noClient = askNoClient();
		Client client = Control.checkNoClient(noClient);
		if (client != null) {
			List<Compte> listCompte = gestionClient.getCompte(client);
			
			int typeCompte = askCompteChoice(listCompte);
			String id_noClient = client.getId_noclient();
			boolean decouvert = askAuthDecouvert();
			if (Control.createCompte(typeCompte, id_noClient, decouvert)) {
				System.out.println("Le compte est bien cr�� !");
			}
		} else {
			System.out.println("Le client recherch� n'existe pas");
			askNoClient();
		}
		

	}
	
	public static void createVirement(List<Compte> listCompte) {
		String noCompte = askNoCompte();
		
		if(Control.analyseNoCompte(noCompte)){
			
			Compte compteCrediteur = gestionCompte.getCompte(noCompte);
			System.out.println(compteCrediteur.getSolde());
			if (compteCrediteur != null) {
				
				int choixCompteDebiteur = displayChoiceClientCompteDebiteur(listCompte);
				float montant = Display.askMontant();
				
				if(Control.checkMontantCompte(montant, listCompte.get(choixCompteDebiteur-1))) {
					GOperation gestionOperation = new GOperation(); 
					
					Operation opeDeb = gestionOperation.creerOperation(montant, listCompte.get(choixCompteDebiteur-1), true);
					Operation opeCred = gestionOperation.creerOperation(montant, compteCrediteur, false);
					if(gestionCompte.virement(listCompte.get(choixCompteDebiteur-1), opeDeb, compteCrediteur, opeCred)) {
						System.out.println("Les "+montant+" euros ont bien �t� cr�dit�s sur le compte correspondant");
					}
				}else{
					plusDThune();
				}
			}
				
			}else {
				displayErrorNoCompte();
			}
		
	}
	
	public static void alimenterCompte(List<Compte> listCompte) {
		
		int choixCompteCrediteur = displayChoiceClientCompte(listCompte);
		float montant = askMontantCred();
		if(Control.alimenterCompte(listCompte, choixCompteCrediteur, montant)) {
			System.out.println("Le compte a bien �t� aliment� de "+montant+" euros");
		}
	}
	
	public static void consultOperationCompte(List<Compte> listCompte){
		int choixCompte = displayChoiceClientCompte(listCompte);
		displayOperation(listCompte.get(choixCompte - 1 ).getOperation());
	}
	
	public static void retrait(List<Compte> listCompte) {
		int choixCompteDebiteur = displayChoiceClientCompteDebiteur(listCompte);
		float montant = askMontant();
		if(Control.checkMontantCompte(montant, listCompte.get(choixCompteDebiteur-1))) {
			if(Control.retrait(listCompte,  choixCompteDebiteur,  montant)) {
				System.out.println("Bonjour Madame la marchande, voici vos "+montant+" eruos !");
			}
		}else {
			plusDThune();
			}
	}

	
	
	/*-------------------------------------------------------METHODE CONSEILLER/ADMIN--------------------------------------------------------------------*/
	
	public static String askAgence() {
		System.out.println("Veuillez indiquer le nom de l'agence sur laquelle vous souhaitez positionner le client");
		Scanner in = new Scanner ( System.in);
		String nomAgence = in.next();
		
		return nomAgence;
	}
	
	public static String askInfoUser() {
		System.out.println("Veuillez indiquer le nom, le pr�nom, la date de naissance (yyyy-mm-dd), le tout s�par� par des ; ");
		Scanner in = new Scanner(System.in);
		String infoClient = in.nextLine();
		
		return infoClient;
	}
	
	/**
	 * Demande le no de client pour faire une action
	 * @return
	 */
	public static String askNoClient() {
		System.out.println("Merci de noter le numero de client pour qui vous souhaitez faire cette action");
		Scanner in = new Scanner(System.in);
		String noClient = in.next();
		return noClient;
		
	}
	
	public static int askCompteChoice(List<Compte> listCompte) {
		int typeCompte = -1;
			System.out.println("Vous avez la possibilit� de cr�er 3 compte :\n"
							 + "1 - Un compte Courant ( 25E de frais par ans\n)"
							 + "2 - Un PEL \n"
							 + "3 - Un Livret A \n"
							 + "\n Merci de choisir le type de compte que vous souhaitez");
			Scanner in = new Scanner(System.in);
			typeCompte = in.nextInt();
			if (listCompte != null) {
				for ( Compte compte: listCompte) {
					System.out.println(compte);
					if ( compte.getId_typecompte() == typeCompte) {
						System.out.println("Le client possede d�j� ce type de compte, merci d'en choisir un qu'il n'a pas");
						askCompteChoice(listCompte);
					}
				}
			}
			
			return typeCompte;
	}
	
	public static boolean askAuthDecouvert() {
		boolean auth = false;
		System.out.println("Autorisation d�couvert : \n"
						 + "1 - Activ� \n"
						 + "0 - D�sactiv� \n");
		Scanner in = new Scanner (System.in);
		int decouvert = in.nextInt();
		if ( decouvert == 1) {
			auth = true;
		}
		
		return auth;
	}
	
	public static String askNoCompteToDesactivate() {
		System.out.println("Merci de taper le numero de compte que vous souhaitez d�sactiver");
		Scanner in = new Scanner(System.in);
		String noCompte = in.next();
		
		return noCompte;	
	}
	
	public static int askActiveOrNot() {
		System.out.println("Souhaitez-vous :"
						 + "1 - Activer \n"
						 + "2 - D�sactiver\n"
						 + "le compte bancaire");
		Scanner in = new Scanner(System.in);
		int activ = in.nextInt();
		
		return activ;	
	}
	
	/**
	 * Change le client d'agence
	 */
	public static void changeAgence() {
		String noClient = askNoClient();
		System.out.println("Tapez le nom de l'agence sur laquelle vous souhaitez switcher le client");
		Scanner in = new Scanner(System.in);
		String agence = in.next();
		if(Control.changeAgence(noClient, agence)) {
			System.out.println("Le client a bien chang� de domiciliation");
		}
		
	}
	
	public static void changeInfoClient() {
		String noClient = askNoClient();
		System.out.println("Que souhaitez vous modifier chez le client ?\n"
						 + " 1 - son nom ?\n"
						 + " 2 - son pr�nom ?\n"
						 + " 3 - sa date de naissance ? (yyyy-mm-dd) ");
		Scanner in = new Scanner(System.in);
		int choix = in.nextInt();
		System.out.println("Merci d'�crire la modification :");
		String champsToUpdate = in.next();
		if (Control.update(noClient, champsToUpdate, choix)) {
			System.out.println("La modification a �t� effectu�e");
		}
		
	}
	
	public static void print(Client client, List<Compte> listCompte){
		Control.print(client, listCompte);
	}
	
	/*------------------------------------------------AFFICHAGE CLIENT PROFIL/COMPTE...--------------------------------------------------------*/
	public static void displayClientProfil(Client c) {
		System.out.println("----------------------------------------\n"
						 + "              VOTRE PROFIL              \n"
						 + "Nom :"+c.getNom()+"                     \n"
						 + "Prenom :"+c.getPrenom()+"               \n"
						 + "Date de Naissance :"+c.getBirthdate()+" \n"
						 + "----------------------------------------\n"
						 );
	}
	
	public static void displayClientCompte(List<Compte> listCompte) {
		for (int i = 0; i <listCompte.size(); i++) {
			System.out.println(listCompte.get(i));
		}
	}
	
	public static int displayChoiceClientCompte(List<Compte> listCompte) {
		System.out.println("Pour quel compte  ? Merci de taper le n� correspondant");
		for ( int i = 0; i<listCompte.size(); i++ ) {
			System.out.println((i+1)+" - "+listCompte.get(i).getId_typecompte());	
		}
		Scanner in = new Scanner(System.in);
		int choix = in.nextInt();
		return choix;
	}
	
	public static void displayOperation(List<Operation> operations) {
		for ( Operation operation : operations) {
			System.out.println(operation);
		}
	}
	
	public static String askNoCompte() {
		System.out.println("Merci de taper le numero de compte sur lequel vous souhaitez faire un virement");
		Scanner in = new Scanner(System.in);
		String noCompte = in.next();
		return noCompte;	
	}
	
	public static int displayChoiceClientCompteDebiteur(List<Compte> listCompte) {
		System.out.println("Quel compte souhaitez vous d�biter ? Merci de taper le n� correspondant");
		for ( int i = 0; i<listCompte.size(); i++ ) {
			System.out.println((i+1)+" - "+listCompte.get(i).getId_typecompte());	
		}
		Scanner in = new Scanner(System.in);
		int choix = in.nextInt();
		return choix;
	}
	
	public static float askMontant() {
		System.out.println("Quel montant souhaitez d�biter de ce compte ?");
		Scanner in = new Scanner(System.in);
		float montant = in.nextFloat();
		return montant;
	}
	
	public static float askMontantCred() {
		System.out.println("Quel montant souhaitez crediter sur ce compte ?");
		Scanner in = new Scanner(System.in);
		float montant = in.nextFloat();
		return montant;
	}
	

	/*--------------------------------------AFFICHAGE ERREUR-----------------------------------------------------------------------------*/
	public static void displayErrorEmptyCompte() {
		System.out.println("Vous n'avez pas encore de compte bancaire, merci de contacter un conseiller pour qu'il vous en cr�� un");
	}
	
	public static void displayErrorNoCompte() {
		System.out.println("Le no de compte est incconu. Merci de recommencer l'op�ration");
		askNoCompte();
	}
	
	public static void plusDThune() {
		System.out.println("Vous n'avez plus un sous ! C'est mort mec");
	}
	
	public static void main(String[] args) {
		new Display();
	}

}
