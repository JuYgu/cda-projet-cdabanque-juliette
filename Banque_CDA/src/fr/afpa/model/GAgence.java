package fr.afpa.model;

import fr.afpa.bean.Agence;
import fr.afpa.dao.AgenceDao;

public class GAgence {

	
	static Agence agence;
	static AgenceDao agenceDao = new AgenceDao();
	
	/**
	 * Appelle le DAO d'agence pour la cr�ation d'une agence dans la base de donn�es
	 * @param paramAgence
	 */
	public boolean createAgence(String paramAgence) {
		String [] param = paramAgence.split(";");
		Agence agence = new Agence(param[0], param[1]);
		return agenceDao.add(agence);
		
	}
	
	/**
	 * Appelle le DAO d'agence pour v�rifier si l'agence existe et renvoyer son id
	 * @param nomAgence
	 * @return
	 */
	public static int[] findIdAgence(String nomAgence) {
		int [] paramVerif = new int [2];
		paramVerif[0] = agenceDao.find(nomAgence);
		if (paramVerif[0] == -1) {
			paramVerif[1] = 0;
		}else {
			paramVerif[1] = 1;
		}
		
		return paramVerif;
	}
	
	
}
