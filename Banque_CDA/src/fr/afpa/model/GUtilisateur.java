package fr.afpa.model;

import java.util.Random;

import fr.afpa.bean.Utilisateur;
import fr.afpa.dao.UtilisateurDao;

public class GUtilisateur implements UtilisateurDao {

	
	Utilisateur user;

	/**
	 * La m�thode retourne l'objet utilisateur
	 * @param login
	 * @return
	 */
	public Utilisateur isExist(String numeroUtilisateur) {
		user = new Utilisateur();
		user.setId_utilisateur(numeroUtilisateur);
		return UtilisateurDao.find(user);
		
	}
	
	
	
	
	/**
	 * Permet l'a cr�ation d'un utilisateur
	 * @param typeU
	 * @return
	 */
	public String createUtilisateur(int typeU) {
			
			String login = createNoUtilisateur(typeU);
			user = new Utilisateur(login, typeU);

			UtilisateurDao.add(user);
			return login;
		}
		
	
	/**
	 * Permet de cr�er l'id utilisateur, v�rifie si elle existe d�j� et recommence si c'est le cas
	 * @param type
	 * @return
	 */
	public String createNoUtilisateur(int type) {
		StringBuilder login =  new StringBuilder();
		String noUtilisateur ="";
		if (type == 1) {
			login.append("ADM");
			for (int i = 0; i < 2; i++) {
				login.append(randInt(0,9));
			}

		} else if (type == 2){
			login.append("CO");
			for (int i = 0; i < 4; i++) {
				login.append(randInt(0,9));
			}
		}else if (type == 3){
			for (int i = 0; i < 10; i++) {
				login.append(randInt(0,9));
			}
		}
		user = new Utilisateur();
		noUtilisateur = login.toString();
		user.setId_utilisateur(noUtilisateur);
		if (UtilisateurDao.find(user) != null ) {
			createNoUtilisateur(type);
		}
		return noUtilisateur;
	}
	
	
	public static int randInt(int min, int max) {
	    Random rand = new Random();
	    int randomNum = rand.nextInt((max - min) + 1) + min;

	    return randomNum;
	}
}
