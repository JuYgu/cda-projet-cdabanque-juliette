package fr.afpa.model;

import java.time.LocalDate;
import java.util.List;
import java.util.Random;

import fr.afpa.bean.Client;
import fr.afpa.bean.Compte;
import fr.afpa.dao.ClientDao;

public class GClient {

	static Client client;
	static ClientDao clientDao = new ClientDao();
	
	/**
	 * Cr�� une client et l'envoie en DAO pour l'int�grer dans la BDD
	 * @param paramClient
	 * @param paramClientId
	 * @return
	 */
	public String createClient(String infoClient, int idAgence, String idUser) {
			String idClient = createIdClient();
			client = new Client(idClient, infoClient.split(";")[0], infoClient.split(";")[1], LocalDate.parse(infoClient.split(";")[2]), idAgence, idUser);
			clientDao.add(client);
			
			return idClient;
		}
	
	
	/**
	 * Cr�� l'id client de mani�re al�atoire et v�rifie dans la BDD si l'id est d�j� existante ou non.
	 * @return
	 */
	public String createIdClient() {
		StringBuilder id_Client = new StringBuilder();
		String alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		for (int i = 0; i < 2; i++) {
			id_Client.append(alpha.charAt(randInt(0,alpha.length()-1)));
		}
		for (int i = 0; i < 6; i++) {
			id_Client.append(randInt(0,9));
		}
	
		String idClient = id_Client.toString();
		client = new Client();
		client.setId_noclient(idClient);
		
		if (clientDao.findById(client) != null) {
			createIdClient();
		}
		return idClient;
	}
	
	
	/**
	 * R�cup�re le client grace a son id Utilisateur
	 * @param codeClient
	 * @return
	 */
	public Client getClient(String codeClient) {
		System.out.println("On va chercher le client grace a son idUtilisateur");
		client = new Client();
		client.setId_utilisateur(codeClient);
		return clientDao.find(client);
	}
	
	/**
	 * Recherche le client dans le DAO pour voir s'il existe bien
	 * @param noClient
	 * @return
	 */
	public static Client getClientById(String noClient) {
		client = new Client();
		client.setId_noclient(noClient);
		System.out.println("noClient");
		System.out.println(client.getId_noclient());
		return clientDao.findById(client);
	}
	
	/**
	 * Premet de r�cup�rer une liste associ�e a un client
	 * @param client
	 * @return
	 */
	public static List<Compte> getCompte(Client client) {
		return clientDao.getCompte(client);
	}
	
	
	/**
	 * Permet d'update un client en fonction du type de requete donn�
	 * @param noClient
	 * @param champToUpdate
	 * @param typeRequete
	 * @return
	 */
	public static boolean update(String noClient, String champToUpdate, int typeRequete ) {
		return clientDao.update(noClient, champToUpdate, typeRequete);
	}
	
	
	
	public static int randInt(int min, int max) {
	    Random rand = new Random();
	    int randomNum = rand.nextInt((max - min) + 1) + min;

	    return randomNum;
	}

}
