package fr.afpa.model;

import java.time.LocalDate;
import java.util.List;
import java.util.Random;

import fr.afpa.bean.Compte;
import fr.afpa.bean.Operation;
import fr.afpa.dao.CompteDao;
import fr.afpa.dao.OperationDao;

public class GCompte {
	
	Compte compte;
	CompteDao compteDao = new CompteDao();
	OperationDao operationDao = new OperationDao();
	
	/**
	 * Permet de cr�er un compte
	 * @param typeCompte
	 * @param id_noClient
	 * @param decouvert
	 * @return
	 */
	public boolean createCompte(int typeCompte, String id_noClient, boolean decouvert) {
		StringBuilder str = new StringBuilder();
		for ( int i = 0; i < 10;i++) {
			str.append(randInt(0,9));
		}
		String idCompte = str.toString();
		
		compte = new Compte (idCompte, 0f, LocalDate.now() ,decouvert, true,  id_noClient, typeCompte);
		return compteDao.add(compte);
		
	}
	
	/**
	 * Permet de r�cup�rer les op�rations d'un compte
	 * @param listCompte
	 * @return
	 */
	public List<Compte> getOperation(List<Compte> listCompte){
		return compteDao.getOperation(listCompte);
	}
	
	public static int randInt(int min, int max) {
	    Random rand = new Random();
	    int randomNum = rand.nextInt((max - min) + 1) + min;

	    return randomNum;
	}
	
	/**
	 * R�cup�re le compte demand� avec le no de compte donn�
	 * @param noCompte
	 * @return
	 */
	public Compte getCompte(String noCompte) {
		Compte newCompte = new Compte(noCompte);
		newCompte = compteDao.find(newCompte);
		return newCompte;
	}
	
	
	/**
	 * Permet de faire un virement et de cr�er des op�rations
	 * @param compteDebiteur
	 * @param opeDeb
	 * @param compteCrediteur
	 * @param opeCred
	 * @return
	 */
	public boolean virement(Compte compteDebiteur, Operation opeDeb, Compte compteCrediteur, Operation opeCred) {
		System.out.println(compteCrediteur.getId_nocompte());
		System.out.println("Op� d�b"+opeDeb.getMontant());
		System.out.println("Op� Cred"+opeCred.getMontant());
		compteDebiteur.setSolde(compteDebiteur.getSolde() -  (opeDeb.getMontant()));
		compteCrediteur.setSolde(compteCrediteur.getSolde() +  opeDeb.getMontant());
		if (compteDao.update(compteDebiteur) && operationDao.add(opeDeb)  && compteDao.update(compteCrediteur) && operationDao.add(opeCred)) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Permet d'approvisionner un compte en particulier
	 * @param compteCrediteur
	 * @param opeCred
	 * @return
	 */
	public boolean appro(Compte compteCrediteur, Operation opeCred) {
		compteCrediteur.setSolde(compteCrediteur.getSolde() +  opeCred.getMontant());
		if (compteDao.update(compteCrediteur) && operationDao.add(opeCred)) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Permet de faire un retrait
	 * @param compteCrediteur
	 * @param opeCred
	 * @return
	 */
	public boolean retrait(Compte compteCrediteur, Operation opeCred) {
		compteCrediteur.setSolde(compteCrediteur.getSolde() -  opeCred.getMontant());
		if (compteDao.update(compteCrediteur) && operationDao.add(opeCred)) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Permet de d�sactiver un compte
	 * @param noCompte
	 * @return
	 */
	public boolean desactiver(String noCompte, int activ) {
		Compte compte = getCompte(noCompte);
		boolean activate = false;
		if ( activ == 1) {
			activate = true;
		}
		compte.setActivation(activate);
		return compteDao.updateActivation(compte);	
	}
}
