package fr.afpa.model;

import fr.afpa.bean.Compte;
import fr.afpa.bean.Operation;

public class GOperation {

	public Operation creerOperation(float montant, Compte compte, boolean isDeb) {
		String type ="";
		if ( isDeb) {
			type = "DEB";
		}else {
			type = "CRED";
		}
		return new Operation(type, montant, compte.getId_nocompte());
	}
}
