package fr.afpa.model;


import java.io.*;
import java.util.List;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import fr.afpa.bean.Client;
import fr.afpa.bean.Compte;




public class Print {
	
	
	
	public static final String DEST = "./pdf/releveCompte.pdf";

	
public static void printPdf(Client client, List<Compte> listCompte) {

		try {
			
			Document document = new Document();
			PdfWriter.getInstance(document, new FileOutputStream(DEST));
			document.open();
			

			
			//insertion info facture et info client
			Paragraph para2 = new Paragraph();

			
			String line4 = "Numero Client  : "+client.getId_noclient()+"\nNom : "+client.getNom()+"\nPrenom : "+client.getPrenom()+" \nDate de Naissance : "+client.getBirthdate().toString()+"\n\n";
			para2.add(line4);
			
			
			//insertion info chambre et prix
			String line5 = "--------------------------------------------------------------------------------------------------- \n"
					     + "LISTE DE COMPTE\n"
					     + "---------------------------------------------------------------------------------------------------\n"
					     + "NUMERO de COMPTE                                      SOLDE                                        \n"
					     + "----------------------------------------------------------------------------------------------------";
			// Variables n�cessaires � l'affichage
			String line6 ="\n";
			String smiley =":-[";
		for(Compte compte : listCompte) {
			if( compte.getSolde() > 0) {
				smiley = ":-]";
			}
			line6 += compte.getId_nocompte()+"                                                  "+compte.getSolde()+"                        "+smiley+"\n";

		}
			
			

		
			
			Paragraph para = new Paragraph();
			para.add(line5);
			para.add(line6);

			
			Paragraph para3 = new Paragraph();

			document.add(para2);
			document.add(para);
			document.add(para3);
			document.close();
			
			System.out.println("Le document a bien �t� g�n�r�e en pdf.");
		
			
		}catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Tu ne rentres pas dans la boucle petit bonhomme");
			e.printStackTrace();
		}
		
		
		

	}
}
