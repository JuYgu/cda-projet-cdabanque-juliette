package fr.afpa.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import fr.afpa.bean.Utilisateur;

public interface UtilisateurDao {

	
	
	public static Utilisateur find(Utilisateur user) {
		Connection conn =null;
		Statement stService = null;
		ResultSet result = null;
		Utilisateur utilisateur = null;
		try {
			//conn = ConnectionBDD.getInstance();
			Class.forName(ConnectionBDD.DRIVER);
			conn = DriverManager.getConnection(ConnectionBDD.URL, ConnectionBDD.LOGIN, ConnectionBDD.PASSWORD) ;
			
			String strQuery = "SELECT u.* FROM utilisateur u where u.id_utilisateur ='"+user.getId_utilisateur()+"'";
			stService = conn.createStatement();
			result  = stService.executeQuery(strQuery);

			if(result.next()) {

				utilisateur = new Utilisateur(result.getString(1), result.getInt(2));
			}
			
		}catch(SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}finally {
			// Fermeture du statement
			if (result != null)
				try {
					result.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if (stService != null) {
				try {
					stService.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
				
			// Fermeture de la connection
			if (conn != null) {
				try {

					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return utilisateur;
	}
	
	
	
	public static int add(Utilisateur t) {
		Statement stAdd = null;
		ResultSet result = null;
		Connection conn = null;
		int id_utilisateur = -1;
	
		try {
			Class.forName(ConnectionBDD.DRIVER);
			conn = DriverManager.getConnection(ConnectionBDD.URL, ConnectionBDD.LOGIN, ConnectionBDD.PASSWORD) ;
			String strQuery ="INSERT INTO utilisateur "
						   + "VALUES ( '"+t.getId_utilisateur()+"', '"+t.getId_typeUtilisateur()+"' )";
			
			stAdd = conn.createStatement();
			stAdd.executeUpdate(strQuery);
					
			
		}catch(SQLException | ClassNotFoundException e) {

			e.printStackTrace();
		}finally {
			// Fermeture du statement
			if (result != null)
				try {
					result.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if (stAdd != null) {
				try {
					stAdd.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
				
			// Fermeture de la connection
			if (conn != null) {
				try {

					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	
		return id_utilisateur;

	}
	
}
