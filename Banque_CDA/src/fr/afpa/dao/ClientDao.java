package fr.afpa.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import fr.afpa.bean.Client;
import fr.afpa.bean.Compte;

public class ClientDao {

	
	/**
	 * R�cup�re un client grace � son id Utilisateur
	 * @param client
	 * @return
	 */
	public Client find(Client client) {
		Statement stService = null;
		ResultSet result = null;
		Client newClient = null;
		Connection conn = null;
		try {
			Class.forName(ConnectionBDD.DRIVER);
			conn = DriverManager.getConnection(ConnectionBDD.URL, ConnectionBDD.LOGIN, ConnectionBDD.PASSWORD) ;
			String strQuery = "SELECT c.* "
							+ "FROM client c "
							+ "INNER JOIN utilisateur u "
							+ "ON c.id_utilisateur = u.id_utilisateur "
							+ "WHERE u.id_utilisateur = '"+client.getId_utilisateur()+"';";
			stService = conn.createStatement();
			result  = stService.executeQuery(strQuery);
			
			while(result.next()) {
				newClient = new Client(result.getString(1), result.getString(2), result.getString(3), (result.getDate(4)).toLocalDate(), result.getInt(5),result.getString(6));
			}
			
			
		}catch(SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}finally {
			// Fermeture du statement
			if (result != null)
				try {
					result.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if (stService != null) {
				try {
					stService.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
				
			// Fermeture de la connection
			if (conn != null) {
				try {

					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return newClient;
	}
	
	/**
	 * R�cup�re un client grace a son ID
	 * @param client
	 * @return
	 */
	public Client findById(Client client) {
		Statement stService = null;
		ResultSet result = null;
		Client newClient = null;
		Connection conn = null;
		try {
			Class.forName(ConnectionBDD.DRIVER);
			conn = DriverManager.getConnection(ConnectionBDD.URL, ConnectionBDD.LOGIN, ConnectionBDD.PASSWORD) ;
			
			System.out.println(client.getId_noclient());
			String strQuery = "SELECT c.* "
							+ "FROM client c "
							+ "WHERE id_noclient = '"+client.getId_noclient()+"'";
			stService = conn.createStatement();
			result  = stService.executeQuery(strQuery);
			
			while(result.next()) {
				newClient = new Client(result.getString(1), result.getString(2), result.getString(3), (result.getDate(4)).toLocalDate(), result.getInt(5),result.getString(6));
			}
			
			
		}catch(SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}finally {
			// Fermeture du statement
			if (result != null)
				try {
					result.close();
				} catch (SQLException e) {
					System.out.println("pb r�sult");
					e.printStackTrace();
				}
			if (stService != null) {
				try {
					stService.close();
				} catch (SQLException e) {
					System.out.println("pb r�sult");
					e.printStackTrace();
				}
			}
				
			// Fermeture de la connection
			if (conn != null) {
				try {

					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return newClient;
	}
	
	/**
	 * Ajoute un client
	 * @param client
	 * @return
	 */
	public  boolean  add(Client client) {
		Statement stAdd = null;
		Connection conn = null;
		boolean ok = true;
		try {
			Class.forName(ConnectionBDD.DRIVER);
			conn = DriverManager.getConnection(ConnectionBDD.URL, ConnectionBDD.LOGIN, ConnectionBDD.PASSWORD) ;
			String strQuery ="INSERT INTO client "
						   + "VALUES( '"+client.getId_noclient()+"', '"+client.getNom()+"', '"+client.getPrenom()+"', '"+Date.valueOf(client.getBirthdate())+"', '"+client.getId_codeagence()+"' , '"+client.getId_utilisateur()+"' )";
			
			stAdd = conn.createStatement();
			stAdd.executeUpdate(strQuery);
		
		}catch(SQLException | ClassNotFoundException e) {
			ok = false;
			e.printStackTrace();
		}finally {
			if (stAdd != null) {
				try {
					stAdd.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			// Fermeture de la connection
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return ok;
	}
	
	public   boolean update(String noClient, String champToUpdate, int typeRequete) {
		Statement stService = null;
		Connection conn = null;
		boolean ok = true;
		String strQuery = "";

		
		try {
			Class.forName(ConnectionBDD.DRIVER);
			conn = DriverManager.getConnection(ConnectionBDD.URL, ConnectionBDD.LOGIN, ConnectionBDD.PASSWORD) ;

			if (typeRequete == 4 ) {
				strQuery = "UPDATE client  "
						+ "SET id_codeagence  = "+Integer.parseInt(champToUpdate)+" "
						+ "WHERE id_noclient= '"+noClient+"'";
			}else if (typeRequete == 1) {
				strQuery = "UPDATE client  "
						+ "SET nom = '"+champToUpdate+"' "
						+ "WHERE id_noclient = '"+noClient+"'";
			}else if (typeRequete ==2) {
				strQuery = "UPDATE client  "
						+ "SET prenom = '"+champToUpdate+"' "
						+ "WHERE id_noclient = '"+noClient+"'";
			}else if(typeRequete == 3) {
				strQuery = "UPDATE client  "
						+ "SET birthdate = '"+Date.valueOf(champToUpdate)+"' "
						+ "WHERE id_noclient = '"+noClient+"'";
			}
			
			stService = conn.createStatement();
			stService.executeUpdate(strQuery);
		}catch(SQLException | ClassNotFoundException e) {
			ok = false;
			e.printStackTrace();
		}finally {
			
			if (stService != null) {
				try {
					stService.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
				
			// Fermeture de la connection
			if (conn != null) {
				try {

					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return ok;
	}
	
	/**
	 * R�cup�re la liste de compte que possede le client
	 * @param client
	 * @return
	 */
	public List<Compte> getCompte(Client client) {
		Statement stService = null;
		ResultSet results = null;
		Connection conn = null;
		List<Compte> newList = new ArrayList<>();
		Compte newCompte = null;
		try {
			Class.forName(ConnectionBDD.DRIVER);
			conn = DriverManager.getConnection(ConnectionBDD.URL, ConnectionBDD.LOGIN, ConnectionBDD.PASSWORD) ;
			String strQuery = "SELECT co.* "
							+ "FROM compte co "
							+ "INNER JOIN client c "
							+ "ON c.id_noclient = co.id_noclient "
							+ "where c.id_noclient = '"+client.getId_noclient()+"'";
			
			stService = conn.createStatement();
			results  = stService.executeQuery(strQuery);
			
			if (results != null) {
				// Utilisation du ResultSet
				while (results.next()) {
					newCompte = new Compte(results.getString(1),
							   results.getFloat(2),
							   (results.getDate(3)).toLocalDate(),
							   results.getBoolean(4),
							   results.getBoolean(5),
							   results.getString(6),
							   results.getInt(7));

					newList.add(newCompte);
				}
					
			}
				
		}catch(SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}finally {
			// Fermeture du statement
			if (results != null)
				try {
					results.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if (stService != null) {
				try {
					stService.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
				
			// Fermeture de la connection
			if (conn != null) {
				try {

					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return newList;
	}
	
	
}
