package fr.afpa.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import fr.afpa.bean.Agence;

public class AgenceDao {

	
	
	public int find(String nomAgence) {
		Connection conn = null;
		Statement stService = null;
		ResultSet result = null;
		int idAgence = -1;
		try {
			conn = ConnectionBDD.getInstance();
			String strQuery = "SELECT * FROM agence where nom ='"+nomAgence+"';";
			stService = conn.createStatement();
			result  = stService.executeQuery(strQuery);
			
			while(result.next()) {
				idAgence = result.getInt(1);
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		
		} finally {
			// Fermeture du statement
			if (result != null)
				try {
					result.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if (stService != null) {
				try {
					stService.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
				
			// Fermeture de la connection
			if (conn != null) {
				try {

					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
			
		
		return idAgence;
	}
	
	
	/**
	 * Permet d'ajouter une agence dans la base donn�es, renvoie un boolean true si l'actionb s'est bien execut�e
	 * @param agence
	 * @return
	 */
	public boolean add(Agence agence) {

		Connection conn = null;
		Statement stAdd = null;
		boolean ok = true;
		
		try {
			// conn = ConnectionBDD.getInstance();
			Class.forName(ConnectionBDD.DRIVER);
			conn = DriverManager.getConnection(ConnectionBDD.URL, ConnectionBDD.LOGIN, ConnectionBDD.PASSWORD) ;
			
			String strQuery = "INSERT INTO agence "
							+ "VALUES ( nextval('seq_agence'),'"+agence.getNom()+"', '"+agence.getAdresse()+"' )";
			
			stAdd = conn.createStatement();
			stAdd.executeUpdate(strQuery);
		}catch(SQLException | ClassNotFoundException e) {
			ok = false;
			e.printStackTrace();
		}finally {
			// Fermeture du statement
			
			if (stAdd != null) {
				try {
					stAdd.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
				
			// Fermeture de la connection
			if (conn != null) {
				try {

					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return ok;
	}
	
	
	
}
