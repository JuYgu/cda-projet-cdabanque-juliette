package fr.afpa.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import fr.afpa.bean.Compte;
import fr.afpa.bean.Operation;

public class CompteDao {

	/**
	 * Permet d'insrer un compte dans la BDD
	 * @param compte
	 * @return
	 */
	public boolean add(Compte compte) {
		Statement stAdd = null;
		boolean ok = true;
		Connection conn = null;
		try {
			Class.forName(ConnectionBDD.DRIVER);
			conn = DriverManager.getConnection(ConnectionBDD.URL, ConnectionBDD.LOGIN, ConnectionBDD.PASSWORD) ;
			String strQuery ="INSERT INTO compte "
						   + "VALUES( '"+compte.getId_nocompte()+"', '"+compte.getSolde()+"', '"+Date.valueOf(compte.getCreationCompte())+"', "+compte.isAuth_decouvert()+", "+compte.isActivation()+",'"+compte.getId_noclient()+"' , "+compte.getId_typecompte()+" )";
			
			stAdd = conn.createStatement();
			stAdd.executeUpdate(strQuery);
	
		}catch(SQLException | ClassNotFoundException e) {
			ok = false;
			e.printStackTrace();
		}finally {

			
			if (stAdd != null) {
				try {
					stAdd.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
				
			// Fermeture de la connection
			if (conn != null) {
				try {

					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return ok;
		
	}
	
	/**
	 * R�cup�re la liste des op�ration de tous les compte d'un client
	 * @param listCompte
	 * @return
	 */
	public List<Compte> getOperation(List<Compte> listCompte){
			
		for (Compte compte : listCompte) {
			Connection conn = null;
			Statement stService = null;
			ResultSet results = null;
			Operation operation = null;
			
			try {
				Class.forName(ConnectionBDD.DRIVER);
				conn = DriverManager.getConnection(ConnectionBDD.URL, ConnectionBDD.LOGIN, ConnectionBDD.PASSWORD) ;
				String strQuery = "SELECT o.* "
								+ "FROM operation o "
								+ "INNER JOIN compte c "
								+ "ON c.id_nocompte = o.id_nocompte "
								+ "WHERE o.id_nocompte = '"+compte.getId_nocompte()+"'";
								
				stService = conn.createStatement();
				results  = stService.executeQuery(strQuery);
				
				// Utilisation du ResultSet
				while (results.next()) {
					operation = new Operation(
										      results.getDate(2),
										      results.getString(3),
										      results.getFloat(4),
										      results.getString(5));

					compte.getOperation().add(operation);
				}
			}catch(SQLException | ClassNotFoundException e) {
				e.printStackTrace();
			}finally {
				// Fermeture du statement
				if (results != null)
					try {
						results.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				if (stService != null) {
					try {
						stService.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
					
				// Fermeture de la connection
				if (conn != null) {
					try {

						conn.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		
		return listCompte;
	}
	
	public Compte find(Compte compte) {
		Statement stService = null;
		ResultSet result = null;
		Connection conn = null;
		Compte newCompte = null;
		
		try {
			Class.forName(ConnectionBDD.DRIVER);
			conn = DriverManager.getConnection(ConnectionBDD.URL, ConnectionBDD.LOGIN, ConnectionBDD.PASSWORD) ;
			String strQuery = "SELECT c.* "
							+ "FROM compte c "
							+ "WHERE c.id_nocompte = '"+compte.getId_nocompte()+"'";
			
			stService = conn.createStatement();
			result  = stService.executeQuery(strQuery);
			
			while(result.next()) {
				newCompte = new Compte(result.getString(1), 
									   result.getFloat(2), 
									   result.getDate(3).toLocalDate(), 
									   result.getBoolean(4), 
									   result.getBoolean(5), 
									   result.getString(6),
									   result.getInt(7));
			}
			
			
		}catch(SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}finally {
			// Fermeture du statement
			if (result != null)
				try {
					result.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if (stService != null) {
				try {
					stService.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
				
			// Fermeture de la connection
			if (conn != null) {
				try {

					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return newCompte;
	}
	
	public boolean update(Compte compte) {
		Statement stUpdate = null;
		ResultSet result = null;
		Connection conn = null;
		boolean ok = true;
		
		try {
			Class.forName(ConnectionBDD.DRIVER);
			conn = DriverManager.getConnection(ConnectionBDD.URL, ConnectionBDD.LOGIN, ConnectionBDD.PASSWORD) ;
			String strQuery = "UPDATE compte "
							+ "SET solde = '"+compte.getSolde()+"' "
							+ "WHERE id_nocompte = '"+compte.getId_nocompte()+"'";
			
			stUpdate = conn.createStatement();
			stUpdate.executeUpdate(strQuery);

		}catch(SQLException | ClassNotFoundException e) {
			ok = false;
			e.printStackTrace();
		}finally {
			// Fermeture du statement
			if (result != null)
				try {
					result.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if (stUpdate != null) {
				try {
					stUpdate.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
				
			// Fermeture de la connection
			if (conn != null) {
				try {

					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return ok;
	}
	
	public boolean updateActivation(Compte compte) {
		Statement stUpdate = null;
		ResultSet result = null;
		boolean ok = true;
		Connection conn = null;
		
		try {
			Class.forName(ConnectionBDD.DRIVER);
			conn = DriverManager.getConnection(ConnectionBDD.URL, ConnectionBDD.LOGIN, ConnectionBDD.PASSWORD) ;
			String strQuery = "UPDATE compte "
							+ "SET activation = '"+compte.isActivation()+"' "
							+ "WHERE id_nocompte = '"+compte.getId_nocompte()+"'";
			
			stUpdate = conn.createStatement();
			stUpdate.executeUpdate(strQuery);

		}catch(SQLException | ClassNotFoundException e) {
			ok = false;
			e.printStackTrace();
		}finally {
			// Fermeture du statement
			if (result != null)
				try {
					result.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if (stUpdate != null) {
				try {
					stUpdate.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
				
			// Fermeture de la connection
			if (conn != null) {
				try {

					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return ok;
	}
	
}
