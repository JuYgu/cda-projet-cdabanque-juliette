package fr.afpa.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;

import fr.afpa.bean.Operation;

public class OperationDao {

	/**
	 *Permet d'ajouter une opération a un compte
	 * @param ope
	 * @return
	 */
	public  boolean  add(Operation ope) {
		Statement stAdd = null;
		boolean ok = true;
		Connection conn = null;
		try {
			Class.forName(ConnectionBDD.DRIVER);
			conn = DriverManager.getConnection(ConnectionBDD.URL, ConnectionBDD.LOGIN, ConnectionBDD.PASSWORD) ;
			String strQuery = "INSERT INTO operation "
							+ "VALUES ( nextval('seq_operation'), '"+LocalDate.now()+"', '"+ope.getType()+"' , "+ope.getMontant()+" , "+ope.getId_nocompte()+" ) ";
		
			stAdd = conn.createStatement();
			stAdd.executeUpdate(strQuery);
		
		}catch(SQLException | ClassNotFoundException e) {
			ok = false;
			e.printStackTrace();
		}finally {
			
			if (stAdd != null) {
				try {
					stAdd.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
				
			// Fermeture de la connection
			if (conn != null) {
				try {

					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return ok;
		
	}
}
