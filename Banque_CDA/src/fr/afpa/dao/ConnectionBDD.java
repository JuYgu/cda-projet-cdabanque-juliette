package fr.afpa.dao;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 * Classe permettant la connexion a la base de donn�e avec un singleton
 * @author NEBULA
 *
 */
public class ConnectionBDD {
	
/*
 * Initialisation des constantes servant a la connexion de la BDD	
 */
	public static final ResourceBundle RB = ResourceBundle.getBundle("fr.afpa.configuration.config");
	public static final String URL = RB.getString("host")+RB.getString("database");
	public static final String LOGIN = RB.getString("login");
	public static final String PASSWORD = RB.getString("password");
	public static final String DRIVER =  RB.getString("driver");
	
	private static Connection conn;
	
	
/**
 * Constructeur initialisant la connexion a la base de donn�e une fois qu'il est instanci�. Il est en priv� pour respecter le pattern singleton afin qu'une seule connxion ne soit possible
 * il faudra appeler la methode getInstance() qui elle m�me appellera ce constructeur pour cr�er une nouvelle connexion.	
 */
	private ConnectionBDD() {
		
		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(URL, LOGIN, PASSWORD);
			
		}catch(SQLException e) {
			System.out.println("jojo");
			e.printStackTrace();
		}catch (ClassNotFoundException e) {
			System.out.println("jj");
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Methode permettant d'instancier le constructeur de la classe ConnectionBDD. C'est cette m�thode qui sera appell�e dans les autres classe.
	 * @return
	 */
	public static Connection getInstance() {
		if(conn == null) {
			new ConnectionBDD();
			System.out.println("Connexion r�ussie");
		}else {
			System.out.println("Il n'y a pas de connexion");
		}
		
		return conn;
	}
	
	

}
