package fr.afpa.bean;

public class Utilisateur {

	private String id_utilisateur;
	private int id_typeUtilisateur;
	
	
	public Utilisateur(String id_utilisateur, int id_typeUtilisateur) {
	
		this.id_utilisateur = id_utilisateur;
		this.id_typeUtilisateur = id_typeUtilisateur;
	}


	public Utilisateur() {
	}


	public String getId_utilisateur() {
		return id_utilisateur;
	}


	public void setId_utilisateur(String id_utilisateur) {
		this.id_utilisateur = id_utilisateur;
	}


	public int getId_typeUtilisateur() {
		return id_typeUtilisateur;
	}


	public void setId_typeUtilisateur(int id_typeUtilisateur) {
		this.id_typeUtilisateur = id_typeUtilisateur;
	}
	
	
	
	
}
