package fr.afpa.bean;

import java.sql.Date;

public class Operation {

	private int id_operation;
	private Date date;
	private String type;
	private float montant;
	private String id_nocompte;
	
	public Operation() {
	}

	public Operation( Date date, String type, float montant, String id_nocompte) {
		this.date = date;
		this.type = type;
		this.montant = montant;
		this.id_nocompte = id_nocompte;
	}
	
	public Operation(String type, float montant, String id_nocompte) {
		this.type = type;
		this.montant = montant;
		this.id_nocompte = id_nocompte;
	}

	public int getId_operation() {
		return id_operation;
	}

	public void setId_operation(int id_operation) {
		this.id_operation = id_operation;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public float getMontant() {
		return montant;
	}

	public void setMontant(float montant) {
		this.montant = montant;
	}

	public String getId_nocompte() {
		return id_nocompte;
	}

	public void setId_nocompte(String id_nocompte) {
		this.id_nocompte = id_nocompte;
	}

	@Override
	public String toString() {
		return "Operation [ date=" + date + ", type=" + type + ", montant=" + montant + "]";
	}
	
	
	
	
	
	
}
