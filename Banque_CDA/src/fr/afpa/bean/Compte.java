package fr.afpa.bean;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import fr.afpa.bean.Operation;

public class Compte {
	private String id_nocompte;
	private float solde;
	private LocalDate creationCompte;
	private boolean auth_decouvert;
	private boolean activation;
	private String id_noclient;
	private int id_typecompte;
	private List<Operation> operation;
	
	public Compte() {
	}
	
	public Compte(String id_noCompte) {
		this.id_nocompte = id_noCompte;
	}

	public Compte(String id_nocompte, float solde, LocalDate creationCompte, boolean auth_decouvert, boolean activation, 
			String id_noclient, int id_typecompte) {
		this.id_nocompte = id_nocompte;
		this.solde = solde;
		this.creationCompte = creationCompte;
		this.auth_decouvert = auth_decouvert;
		this.activation = activation;
		this.id_noclient = id_noclient;
		this.id_typecompte = id_typecompte;
		this.operation = new ArrayList<>();
	}

	public String getId_nocompte() {
		return id_nocompte;
	}

	public void setId_nocompte(String id_nocompte) {
		this.id_nocompte = id_nocompte;
	}

	public float getSolde() {
		return solde;
	}

	public void setSolde(float solde) {
		this.solde = solde;
	}

	public boolean isAuth_decouvert() {
		return auth_decouvert;
	}

	public void setAuth_decouvert(boolean auth_decouvert) {
		this.auth_decouvert = auth_decouvert;
	}

	public boolean isActivation() {
		return activation;
	}

	public void setActivation(boolean activation) {
		this.activation = activation;
	}
	
	
	public List<Operation> getOperation() {
		return operation;
	}

	public void setOperation(List<Operation> operation) {
		this.operation = operation;
	}

 
	
	public String getId_noclient() {
		return id_noclient;
	}

	public LocalDate getCreationCompte() {
		return creationCompte;
	}

	public void setCreationCompte(LocalDate creationCompte) {
		this.creationCompte = creationCompte;
	}

	public int getId_typecompte() {
		return id_typecompte;
	}
	


	public void setId_noclient(String id_noclient) {
		this.id_noclient = id_noclient;
	}

	public void setId_typecompte(int id_typecompte) {
		this.id_typecompte = id_typecompte;
	}

	@Override
	public String toString() {
		return "Compte [id_nocompte=" + id_nocompte + ", solde=" + solde + ", creationCompte=" + creationCompte
				+ ", auth_decouvert=" + auth_decouvert + ", activation=" + activation + ", id_noclient=" + id_noclient
				+ ", id_typecompte=" + id_typecompte + "]";
	}


	
}
