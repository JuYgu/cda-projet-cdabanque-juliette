package fr.afpa.bean;

public class Agence {
	private int id_codeAgence;
	private String nom;
	private String adresse;
	
	
	public Agence(int id_codeAgence, String nom, String adresse) {
		this.id_codeAgence = id_codeAgence;
		this.nom = nom;
		this.adresse = adresse;
	}


	public Agence( String nom, String adresse) {
		this.nom = nom;
		this.adresse = adresse;
	}

	
	public Agence() {
	}


	
	public int getId_codeAgence() {
		return id_codeAgence;
	}


	public void setId_codeAgence(int id_codeAgence) {
		this.id_codeAgence = id_codeAgence;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getAdresse() {
		return adresse;
	}


	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
}
