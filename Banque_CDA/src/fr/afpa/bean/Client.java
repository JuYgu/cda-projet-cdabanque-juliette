package fr.afpa.bean;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

public class Client {

		private String id_noclient;
		private String nom;
		private String prenom;
		private LocalDate birthdate;
		private int id_codeagence;
		private String id_utilisateur;
		//private List <Compte> listeComptes;
		
		
		public Client() {
		}

		public Client(String id_noclient, String nom, String prenom, LocalDate birthdate, int id_codeagence, String id_utilisateur) {
			this.id_noclient = id_noclient;
			this.nom = nom;
			this.prenom = prenom;
			this.birthdate = birthdate;
			this.id_codeagence = id_codeagence;
			this.id_utilisateur = id_utilisateur;
		}
		
		public Client( String nom, String prenom, LocalDate birthdate, int id_codeagence, String id_utilisateur) {
			this.nom = nom;
			this.prenom = prenom;
			this.birthdate = birthdate;
			this.id_codeagence = id_codeagence;
			this.id_utilisateur = id_utilisateur;
		}


		public String getNom() {
			return nom;
		}

		public void setNom(String nom) {
			this.nom = nom;
		}

		public String getPrenom() {
			return prenom;
		}

		public void setPrenom(String prenom) {
			this.prenom = prenom;
		}

		public LocalDate getBirthdate() {
			return birthdate;
		}

		public void setBirthdate(LocalDate birthdate) {
			this.birthdate = birthdate;
		}

//		public List<Compte> getListeComptes() {
//			return listeComptes;
//		}
//
//		public void setListeComptes(List<Compte> listeComptes) {
//			this.listeComptes = listeComptes;
//		}

		public String getId_noclient() {
			return id_noclient;
		}

		public void setId_noclient(String id_noclient) {
			this.id_noclient = id_noclient;
		}

		
		public int getId_codeagence() {
			return id_codeagence;
		}

		public String getId_utilisateur() {
			return id_utilisateur;
		}
		

		public void setId_codeagence(int id_codeagence) {
			this.id_codeagence = id_codeagence;
		}

		public void setId_utilisateur(String id_utilisateur) {
			this.id_utilisateur = id_utilisateur;
		}


}
