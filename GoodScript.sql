
------------------------------------------------------------
--DATABASE
------------------------------------------------------------
create database Banque_CDA;


------------------------------------------------------------
--ROLE
------------------------------------------------------------
create role testCda password '123' SUPERUSER CREATEDB CREATEROLE inherit LOGIN;

-------------------------------------------------------------
--Table: TypeUtilisateur
-------------------------------------------------------------

CREATE TABLE TypeUtilisateur(
        id_typeutilisateur Int primary key ,
        nom                Varchar (50) NOT NULL
);
CREATE SEQUENCE IF NOT EXISTS seq_typeutilisateur;

-------------------------------------------------------------
--Table: utilisateur
-------------------------------------------------------------

CREATE TABLE utilisateur(
        id_utilisateur     Varchar (11) NOT NULL primary Key,
        id_typeutilisateur Int NOT NULL

	,CONSTRAINT utilisateur_TypeUtilisateur_FK FOREIGN KEY (id_typeutilisateur) REFERENCES TypeUtilisateur(id_typeutilisateur)
);

-------------------------------------------------------------
--Table: Agence
-------------------------------------------------------------

CREATE TABLE IF NOT EXISTS Agence(
        id_codeagence numeric(3) primary key ,
        nom           Varchar (10) NOT NULL ,
        adresse       Varchar (50) NOT NULL
);

CREATE SEQUENCE IF NOT EXISTS seq_agence;

-------------------------------------------------------------
--Table: Client
-------------------------------------------------------------

CREATE TABLE IF NOT EXISTS Client(
        id_noclient    varchar(8) primary key ,
        nom            Varchar (50) NOT NULL ,
        prenom         Varchar (50) NOT NULL ,
        birthdate      Date NOT NULL ,
        id_codeagence  Int NOT NULL ,
        id_utilisateur Varchar(11) NOT NULL

	,CONSTRAINT Client_Agence_FK FOREIGN KEY (id_codeagence) REFERENCES Agence(id_codeagence)
	,CONSTRAINT Client_utilisateur0_FK FOREIGN KEY (id_utilisateur) REFERENCES utilisateur(id_utilisateur)
);

------------------------------------------------------------
--Table: TypeCompte
------------------------------------------------------------

CREATE TABLE IF NOT EXISTS TypeCompte(
        id_typecompte Int primary key ,
        nom           Varchar (50) NOT NULL
);

CREATE SEQUENCE IF NOT EXISTS seq_typecompte;
-------------------------------------------------------------
--Table: Compte
-------------------------------------------------------------

CREATE TABLE IF NOT EXISTS Compte(
        id_nocompte    varchar(10) primary key ,
        solde          Float NOT NULL ,
        creationcompte date not null,
        auth_decouvert Bool NOT NULL ,
        activation     Bool NOT NULL ,
        id_noclient    varchar(8) NOT NULL ,
        id_typecompte  Int NOT NULL

	,CONSTRAINT Compte_Client0_FK FOREIGN KEY (id_noclient) REFERENCES Client(id_noclient)
	,CONSTRAINT Compte_TypeCompte1_FK FOREIGN KEY (id_typecompte) REFERENCES TypeCompte(id_typecompte)
);


-------------------------------------------------------------
--Table: Operation
-------------------------------------------------------------

CREATE TABLE IF NOT EXISTS Operation(
        id_operation   Int primary key ,
        date           Date NOT NULL ,
        type 		   varchar(20) NOT NULL ,
        montant        Float NOT NULL ,
        id_nocompte    varchar(11) not null

	,CONSTRAINT Compte_Operation_FK FOREIGN KEY (id_nocompte) REFERENCES Compte(id_nocompte)
);

CREATE SEQUENCE IF NOT EXISTS seq_operation;

insert into typeutilisateur values(nextval('seq_typeutilisateur'), 'ADMIN'),(nextval('seq_typeutilisateur'), 'CONSEILLER'),(nextval('seq_typeutilisateur'), 'CLIENT');
insert into utilisateur values('ADM01',1);
insert into typecompte values(nextval('seq_typecompte'), 'CC'),(nextval('seq_typecompte'), 'PEL'),(nextval('seq_typecompte'), 'LA');